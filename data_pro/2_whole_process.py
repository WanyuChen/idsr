# -*- coding: utf-8 -*-
import pandas as pd
import csv
import numpy as np
import pickle
import argparse

TEST_SPLIT = 0.8
VALID_SPLIT = 0.5
USER_KEY = 'account'
ITEM_KEY = 'item_id'
TIME_KEY = 'time'


def remap_columns(data, c):
    """Remap the values in each to the fields in `columns` to the range [1, number of unique values]"""
    uniques = data[c].unique()
    col_map = pd.Series(index=uniques, data=np.arange(1, len(uniques) + 1))
    str = c
    data[str] = col_map[data[c]].values
    return data, col_map


#####-----------For DSR-----------###############
def last_percentage_out_split(data, dataa, split=0.8,
                              clean_test=True,
                              filtering_len=True,
                              min_session_length=3):
    """
    last_percentage_out_split
    splits actions done by a user to the train and test set with a split based on the sequence length
    """
    train_indices = []
    test_indices = []

    for key, user_seq in data.groupby(USER_KEY):
        split_point = int(split * len(user_seq.index))
        tmp_train_indices = user_seq[:split_point].index.tolist()
        tmp_test_indices = user_seq[split_point:].index.tolist()
        if len(tmp_train_indices) >= min_session_length:
            train_indices.extend(user_seq[:split_point].index.tolist())
        if len(tmp_test_indices) >= min_session_length:
            test_indices.extend(user_seq[split_point:].index.tolist())

    train = dataa.iloc[train_indices]
    test = dataa.iloc[test_indices]

    # Keep only the test sequences where all items occur in the training set
    if clean_test:
        train_items = train[ITEM_KEY].unique()
        print("Before filtering test length is", len(test))
        to_remove = test.loc[~test[ITEM_KEY].isin(train_items)]
        to_remove = to_remove[USER_KEY].unique()
        test = test[~test[USER_KEY].isin(to_remove)]
        print("After filtering items that occur in training, length is", len(test))

        #  remove user sequences in test shorter than min_session_length
    if filtering_len:
        filtered_test_indices = []
        for key, user_seq in test.groupby(USER_KEY):
            tmp_test_indices = user_seq.index.tolist()
            if len(tmp_test_indices) >= min_session_length:
                filtered_test_indices.extend(user_seq.index.tolist())
        test = dataa.iloc[filtered_test_indices]
        print("After filtering short sequences, length is", len(test))

    return train, test


def get_user_sequences_from_df(data):
    dic_DSR = list(data.groupby(USER_KEY))
    sequence_DSR = []
    for i in range(len(dic_DSR)):
        sub_seq_DSR = []
        account = dic_DSR[i][0]
        item = list(dic_DSR[i][1][ITEM_KEY])
        time = list(dic_DSR[i][1][TIME_KEY])

        sub_seq_DSR += (account, item, time)
        sequence_DSR.append(sub_seq_DSR)

    return sequence_DSR


def DSR(click_seq, max_len):
    user_sequences = []
    targets = []
    users = []
    for i in range(len(click_seq)):
        acc_seq = click_seq[i]
        user_id = acc_seq[0]
        item = acc_seq[1]
        tmp_max_len = max_len

        # if the item sequence is too short
        if len(item) <= max_len:
            tmp_max_len = len(item)

        for j in range(1, tmp_max_len - 1):
            target = item[j + 1]
            targets.append([target])

            users.append([user_id])
            user_sequence = np.array(item[0:j + 1], dtype=np.int32)
            user_sequence = list(user_sequence)
            user_sequences.append(user_sequence)
        # if longer we make more windows
        if len(item) > max_len:
            for j in range(1, len(item) - max_len + 1):
                target = item[j + max_len - 1]
                targets.append([target])

                users.append([user_id])
                user_sequences.append(item[j:j + max_len - 1])

    return user_sequences, users, targets


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", default="data/ML100K_account.csv", type=str)  # ML1M_account.csv
    parser.add_argument("--item_file", default="data/u.item", type=str)
    parser.add_argument("--output_train", default="data/train.csv", type=str)
    parser.add_argument("--output_valid", default="data/valid.csv", type=str)
    parser.add_argument("--output_test", default="data/test.csv", type=str)
    parser.add_argument("--maps", default="data/maps.csv", type=str)
    parser.add_argument("--seed", default=42, type=int)
    parser.add_argument("--max_len", default=10, type=int)
    args = parser.parse_args()
    MAX_LEN = args.max_len
    np.random.seed(args.seed)

    print(args)

    S_account = pd.read_csv(args.input_file, usecols=[USER_KEY, ITEM_KEY, TIME_KEY])
    S_account = S_account.sort_values(by=[USER_KEY, TIME_KEY])
    item_genre = pd.read_table(args.item_file, sep='|', header=None, encoding="ISO-8859-1")

    u_max_num = S_account.account.nunique()
    v_max_num = S_account.item_id.nunique()
    print('total user:%d' % u_max_num)
    print('total item:%d' % v_max_num)

    S_account, _ = remap_columns(S_account, USER_KEY)
    S_account, col_map = remap_columns(S_account, ITEM_KEY)
    col_map = pd.DataFrame({'new_id': col_map.values, ITEM_KEY: col_map.index})
    col_map = pd.merge(col_map, item_genre, left_on=ITEM_KEY, right_on=0).drop(0, axis=1)
    col_map.to_csv(args.maps, index=False)

    train_df, test_df_0 = last_percentage_out_split(S_account, S_account, split=TEST_SPLIT)
    valid_df, test_df = last_percentage_out_split(test_df_0, S_account, split=VALID_SPLIT, clean_test=False,
                                                  filtering_len=False, min_session_length=3)

    u_max_num = train_df.account.nunique()
    v_max_num = train_df.item_id.nunique()
    print('total user:%d' % u_max_num)
    print('total item:%d' % v_max_num)

    train_sequences = get_user_sequences_from_df(train_df)
    valid_sequences = get_user_sequences_from_df(valid_df)
    test_sequences = get_user_sequences_from_df(test_df)

    # tuple of sequence, user, item
    train_seq_windowed, train_accounts, train_targets = DSR(train_sequences, MAX_LEN)
    valid_seq_windowed, valid_accounts, valid_targets = DSR(valid_sequences, MAX_LEN)
    test_seq_windowed, test_accounts, test_targets = DSR(test_sequences, MAX_LEN)

    train = (train_seq_windowed, train_accounts, train_targets)
    valid = (valid_seq_windowed, valid_accounts, valid_targets)
    test = (test_seq_windowed, test_accounts, test_targets)

    f1 = open('data/train', 'wb')
    pickle.dump(train, f1)
    f1.close()

    f2 = open('data/valid', 'wb')
    pickle.dump(valid, f2)
    f2.close()

    f3 = open('data/test', 'wb')
    pickle.dump(test, f3)
    f3.close()
    print('Done')
