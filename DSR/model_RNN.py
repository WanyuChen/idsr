from module import get_item_embeddings, self_attention, new_softmax, tensor_ILD, ln, noam_scheme,warmup_scheme
from tqdm import tqdm
import logging
import tensorflow as tf
import copy
from tensorflow.nn.rnn_cell import GRUCell
import random
from numpy.random import seed
seed(42)
tf.compat.v1.set_random_seed(42)
random.seed(42)

class DSR_RNN():
    def __init__(self, args):
        self.arg = args
        self.item_emb_table = get_item_embeddings(self.arg.item_num + 1, self.arg.hidden_units)  

    def encoder(self, xs, training=True):
        with tf.variable_scope("encoder", reuse=tf.AUTO_REUSE):
            seq = tf.nn.embedding_lookup(self.item_emb_table, xs)
            seq_len = tf.cast(tf.reduce_sum(tf.to_int32(tf.not_equal(xs, 0)), -1), dtype=tf.int32)
            enc_output, state = tf.nn.dynamic_rnn(GRUCell(self.arg.enc_units), inputs=seq, sequence_length=seq_len,
                                                  dtype=tf.float32)
            state1 = tf.reshape(state, [tf.shape(state)[0], 1, self.arg.enc_units])
            xsss = tf.to_float(tf.not_equal(xs, 0))  
            xsss = tf.reshape(xsss, [tf.shape(xsss)[0], tf.shape(xsss)[1], 1])
            enc_output = enc_output * xsss  
            ln_state1 = ln(state1)

        with tf.variable_scope("role_att1", reuse=tf.AUTO_REUSE):
            role_pre01, A1, V1,Q1 = self_attention(xsss, ln_state1, enc_output, enc_output, self.arg.enc_units,
                                                self.arg.self_attention_mode,
                                                scope="role_att1_self_attention")  
            role_pre1 = tf.concat((role_pre01, ln_state1), -1)

        with tf.variable_scope("role_att2", reuse=tf.AUTO_REUSE):
            role_pre02, A2, V2,Q2 = self_attention(xsss, ln_state1, enc_output, enc_output, self.arg.enc_units,
                                                self.arg.self_attention_mode,
                                                scope="role_att2_self_attention")  
            role_pre2 = tf.concat((role_pre02, ln_state1), -1)

        with tf.variable_scope("role_att3", reuse=tf.AUTO_REUSE):
            role_pre03, A3, V3,Q3 = self_attention(xsss, ln_state1, enc_output, enc_output, self.arg.enc_units,
                                                self.arg.self_attention_mode,
                                                scope="role_att3_self_attention")  
            role_pre3 = tf.concat((role_pre03, ln_state1), -1)

        with tf.variable_scope("NARM", reuse=tf.AUTO_REUSE):
            W_w = tf.get_variable('W_w', dtype=tf.float32, shape=(self.arg.enc_units, self.arg.enc_units),
                                  initializer=tf.contrib.layers.xavier_initializer())
            role_pref0 = tf.concat((role_pre01, role_pre02, role_pre03), 1)  
            role_pref01 = tf.reshape(role_pref0, [-1, tf.shape(role_pref0)[2]])  
            role_pref02 = tf.matmul(role_pref01, W_w)
            role_pref03 = tf.reshape(role_pref02, [tf.shape(role_pref0)[0], tf.shape(role_pref0)[1], -1])
            role_weight0 = tf.nn.softmax(tf.matmul(ln_state1, tf.transpose(role_pref03, [0, 2, 1])), -1) 

            W_a = tf.get_variable('W_a', dtype=tf.float32, shape=(self.arg.hidden_units, 2 * self.arg.enc_units),initializer=tf.contrib.layers.xavier_initializer())
            emb2 = tf.matmul(self.item_emb_table, W_a)  
            role_pref = tf.concat((role_pre1, role_pre2, role_pre3), 1)  
            role_pref1 = tf.reshape(role_pref, [-1, tf.shape(role_pref)[2]])  
            role_rele = tf.matmul(role_pref1, tf.transpose(emb2)) 
            role_rele1 = tf.nn.softmax(tf.reshape(role_rele, [tf.shape(role_pref)[0], tf.shape(role_pref)[1], -1]),-1)

            user_rele2 = tf.matmul(role_weight0, role_rele1)  
            user_rele3 = tf.reshape(user_rele2, [tf.shape(user_rele2)[0], -1])  
            s_rel = new_softmax(user_rele3)  

            sub_pre = tf.concat((V1, V2, V3), 1)
            pos_pre = tf.concat((A1, A2, A3), 1)

        return role_pref0, s_rel, sub_pre, pos_pre, role_weight0

    def decoder(self, role_pre, s_rel, y_out, y_out_greedy, role_weight, kk, training=True):
        with tf.variable_scope("decoder", reuse=tf.AUTO_REUSE):
            emb1 = self.item_emb_table
            emb1_stop = emb1
            role_pre_stop = role_pre
            role_weight_stop0 = role_weight
            ##---------------------------------------dec GRU---------------------------------------#############
            dec_seq = tf.nn.embedding_lookup(self.item_emb_table, y_out) 
            seq_len = tf.cast(tf.reduce_sum(tf.to_int32(tf.not_equal(y_out, 0)), -1), dtype=tf.int32)
            dec_output, state = tf.nn.dynamic_rnn(GRUCell(self.arg.enc_units), inputs=dec_seq, sequence_length=seq_len,
                                                  dtype=tf.float32)
            dec_pre = ln(state)
            ##---------------------------------------diversity score---------------------------------------#############
            role_pref01 = tf.reshape(role_pre_stop, [-1, tf.shape(role_pre)[2]]) 
            dW_A = tf.get_variable('dW_A', dtype=tf.float32, shape=(self.arg.enc_units, self.arg.enc_units),
                                   initializer=tf.contrib.layers.xavier_initializer())
            dW_B = tf.get_variable('dW_B', dtype=tf.float32, shape=(self.arg.enc_units, self.arg.enc_units),
                                   initializer=tf.contrib.layers.xavier_initializer())
            dW_V = tf.get_variable('dW_V', dtype=tf.float32, shape=(1, self.arg.enc_units),
                                   initializer=tf.contrib.layers.xavier_initializer())
            l11 = tf.reshape(tf.matmul(dW_A, tf.transpose(role_pref01)),
                             [-1, tf.shape(role_pre)[0], tf.shape(role_pre)[1]])  
            l22 = tf.reshape(tf.matmul(dW_B, tf.transpose(dec_pre)), [-1, tf.shape(role_pre)[0], 1]) 
            l22 = tf.tile(l22, [1, 1, tf.shape(role_pre)[1]])  
            l33 = tf.reshape(tf.keras.backend.hard_sigmoid(l11 + l22), [tf.shape(l22)[0], -1])  
            q_jh = tf.reshape(tf.matmul(dW_V, l33), [tf.shape(role_pre)[0],1, tf.shape(role_pre)[1]])  

            e_q_jh = tf.reshape(tf.exp(q_jh)*role_weight_stop0,[tf.shape(q_jh)[0],-1])
            role_weight0 = 1 - new_softmax(e_q_jh)
            role_weight_att = tf.reshape(role_weight0, [tf.shape(q_jh)[0], 1, -1])

            role_pre_stop1 = tf.reshape(role_pre_stop, [-1, tf.shape(role_pre)[2]])  
            role_pre_rele = tf.matmul(role_pre_stop1, tf.transpose(emb1_stop))  
            role_pre_rele1 = tf.nn.softmax(
                tf.reshape(role_pre_rele, [tf.shape(role_pre)[0], tf.shape(role_pre)[1], -1]), -1) 

            s_div = tf.matmul(role_weight_att, role_pre_rele1)  
            s_div = tf.reshape(s_div, [tf.shape(s_div)[0], -1])  
            s_div = new_softmax(s_div)  
            ##---------------------------------------total score---------------------------------------#############
            s_rel_1 = s_rel
            s_div_1 = s_div

            s_rel_stop = s_rel
            score_0 = self.arg.lamb1 * s_rel_stop + (1 - self.arg.lamb1) * s_div  
            score_0 = new_softmax(score_0)
            score1 = score_0 - (
                    tf.to_float(tf.reduce_sum(tf.one_hot(y_out, tf.shape(score_0)[-1]), 1)) * score_0) 

            y_hat0 = tf.to_int32(tf.argmax(score1, axis=-1))  
            y_hat1 = tf.reshape(tf.to_float(tf.one_hot(y_hat0, self.arg.item_num + 1)),
                                [tf.shape(score_0)[0], tf.shape(score_0)[1]]) 
            y_hat_score0 = tf.reduce_sum(score_0 * y_hat1, -1)  
            y_hat_score = tf.reshape(y_hat_score0, [tf.shape(y_hat0)[0], 1]) 
            y_hat = tf.reshape(y_hat0, [-1, 1])
            ###-----------------------------greedy selection---------------------------########
            if training:
                ss_rel = s_rel - (tf.to_float(tf.reduce_sum(tf.one_hot(y_out_greedy, tf.shape(s_rel)[-1]), 1)) * s_rel)
                y_hat0_greedy = tf.to_int32(tf.argmax(ss_rel, axis=-1))  
                y_hat01_greedy = tf.to_float(tf.one_hot(y_hat0_greedy, self.arg.item_num + 1))  
                y_hat1_greedy = tf.reshape(y_hat01_greedy, [tf.shape(score_0)[0], tf.shape(score_0)[1]])
                y_hat_socre0_greedy = tf.reduce_sum(score_0 * y_hat1_greedy, -1)  
                y_hat_score_greedy = tf.reshape(y_hat_socre0_greedy, [tf.shape(y_hat0_greedy)[0], 1])  
                y_hat_greedy = tf.reshape(y_hat0_greedy, [-1, 1])

            if training:
                return y_hat, y_hat_greedy, state, s_rel_1, s_div_1, y_hat_score, y_hat_score_greedy
            else:
                return y_hat, state, s_rel_1, s_div_1

    def train(self, xs, y_ground):

        role_pre, s_rel, sub_pre, pos_pre, role_weight0 = self.encoder(xs)
        y_hat = tf.reshape(tf.to_int32(tf.argmax(s_rel, axis=-1)), [-1, 1]) 

        y_out = y_hat
        y_out_greedy = y_hat

        y_hat1 = tf.reshape(tf.to_float(tf.one_hot(y_hat, self.arg.item_num + 1)),
                            [tf.shape(s_rel)[0], tf.shape(s_rel)[1]])
        y_hat_score0 = tf.reduce_sum(s_rel * y_hat1, -1)
        y_hat_score = tf.reshape(y_hat_score0, [tf.shape(y_hat)[0], 1])
        div_scores = y_hat_score
        div_scores_greedy = y_hat_score

        for kk in range(self.arg.k - 1):
            y_hat, y_hat_greedy, dec_hidden, s_rel_1, s_div_1, y_hat_score, y_hat_score_greedy = self.decoder(role_pre,
                                                                                                              s_rel,
                                                                                                              y_out,
                                                                                                              y_out_greedy,
                                                                                                              role_weight0,kk)  
            y_out = tf.concat((y_out, y_hat), 1)
            y_out_greedy = tf.concat((y_out_greedy, y_hat_greedy), 1)
            div_scores = tf.concat((div_scores, y_hat_score), 1)
            div_scores_greedy = tf.concat((div_scores_greedy, y_hat_score_greedy), 1)

        ys = y_out  
        ys_greedy = y_out_greedy
        div_score = div_scores  
        div_score_greedy = div_scores_greedy  


        # ########################------------------loss relevance-0---------------------######################

        y_ground1 = tf.reshape(tf.one_hot(y_ground, self.arg.item_num + 1), [tf.shape(y_ground)[0], -1])  
        CE_loss_pos = -tf.reduce_sum((tf.log(s_rel + 1e-24) * y_ground1), -1)  
        loss_rel = CE_loss_pos

        # ########################------------------loss diversity 1---------------------######################
        greedy_ILD = tensor_ILD(ys_greedy)
        ys_ILD = tensor_ILD(ys)
        prob_ys = tf.reduce_sum(tf.log(div_score + 1e-24), -1)
        prob_ys_greedy = tf.reduce_sum(tf.log(div_score_greedy + 1e-24), -1)
        prob = 1.0 / (1.0 + tf.exp(prob_ys_greedy - prob_ys))

        pos = tf.ones((tf.shape(ys_ILD)), tf.float32)
        neg = tf.zeros((tf.shape(ys_ILD)), tf.float32)

        ispositive = tf.where(tf.greater((ys_ILD - greedy_ILD), 0), pos, neg)
        loss_div = -(ys_ILD - greedy_ILD) * (
                ispositive * tf.log(prob + 1e-24) + (1 - ispositive) * tf.log(prob + 1e-24))  

        ########################------------------disaggrement loss positon---------------------######################

        pos_pre_0 = tf.reshape(pos_pre[:, 0, :], [tf.shape(pos_pre[:, 0, :])[0], 1, tf.shape(pos_pre[:, 0, :])[1]])
        pos_pre_1 = tf.reshape(pos_pre[:, 1, :], [tf.shape(pos_pre[:, 1, :])[0], 1, tf.shape(pos_pre[:, 1, :])[1]])
        pos_pre_2 = tf.reshape(pos_pre[:, 2, :], [tf.shape(pos_pre[:, 2, :])[0], 1, tf.shape(pos_pre[:, 2, :])[1]])
        ele_1 = tf.to_float(tf.tile(pos_pre_0, [1, tf.shape(pos_pre)[1], 1]))  
        ele_2 = tf.to_float(tf.tile(pos_pre_1, [1, tf.shape(pos_pre)[1], 1])) 
        ele_3 = tf.to_float(tf.tile(pos_pre_2, [1, tf.shape(pos_pre)[1], 1])) 

        pos_pre = tf.to_float(pos_pre)

        ele_1_prod_norm = tf.sqrt(tf.reduce_sum(tf.square(ele_1), axis=2))
        ele_2_prod_norm = tf.sqrt(tf.reduce_sum(tf.square(ele_2), axis=2))
        ele_3_prod_norm = tf.sqrt(tf.reduce_sum(tf.square(ele_3), axis=2))
        pos_pre_norm = tf.sqrt(tf.reduce_sum(tf.square(pos_pre), axis=2))

        ele_1_x = tf.reduce_sum(tf.multiply(ele_1, pos_pre), axis=2)
        ele_2_x = tf.reduce_sum(tf.multiply(ele_2, pos_pre), axis=2)
        ele_3_x = tf.reduce_sum(tf.multiply(ele_3, pos_pre), axis=2)

        ele_1_prod = ele_1_x / (ele_1_prod_norm * pos_pre_norm)
        ele_2_prod = ele_2_x / (ele_2_prod_norm * pos_pre_norm)
        ele_3_prod = ele_3_x / (ele_3_prod_norm * pos_pre_norm)

        loss_dist_pos = tf.reduce_sum((ele_1_prod + ele_2_prod + ele_3_prod), -1) / 9.0  

        ########################------------------ME loss---------------------######################
        role_weig = tf.reshape(role_weight0, [tf.shape(role_weight0)[0], -1])  
        loss_ME = tf.reduce_sum((tf.log(role_weig+1e-24) * role_weig), -1)/2

        ########################------------------total loss---------------------######################
        loss = self.arg.lamb2 * loss_rel + loss_div + loss_ME + self.arg.pos * loss_dist_pos
        loss = tf.reduce_mean(loss)

        global_step = tf.Variable(0, trainable=False)
        optimizer = tf.train.AdamOptimizer(learning_rate=self.arg.lr, beta2=0.999)
        train_op = optimizer.minimize(loss, global_step=global_step)

        tf.summary.scalar('lr', self.arg.lr)
        tf.summary.scalar("loss", loss)
        tf.summary.scalar("global_step", global_step)

        summaries = tf.summary.merge_all()

        return role_weig, pos_pre, s_rel_1, s_div_1, loss, loss_rel, loss_div, loss_dist_pos, loss_ME, train_op, global_step, summaries

    def eval(self, xs, yss):
        y_true = yss
        xs_true = xs

        role_pre, s_rel, sub_pre, pos_pre, role_weight0 = self.encoder(xs, training=False)
        y_hat = tf.reshape(tf.to_int32(tf.argmax(s_rel, axis=-1)), [-1, 1])  ##B,1

        y_out = y_hat
        y_out_greedy = y_hat

        for kk in range(self.arg.k - 1):
            y_hat, dec_hidden, s_rel_1_e, s_div_1_e = self.decoder(role_pre, s_rel,
                                                                   y_out, y_out_greedy, role_weight0,kk,
                                                                   training=False)  
            y_out = tf.concat((y_out, y_hat), 1)

        ys = y_out  
        summaries = tf.summary.merge_all()
        return ys, y_true, xs_true, s_rel_1_e, s_div_1_e, summaries
