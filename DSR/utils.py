import copy
import numpy as np
from sklearn.metrics.pairwise import pairwise_distances
import json
import os, re
import logging
import tensorflow as tf
from numpy.random import seed
seed(42)
tf.compat.v1.set_random_seed(42)
import random
random.seed(42)

def evaluate(hypotheses,y_true,item_genre):
    hh = copy.deepcopy(hypotheses)
    y_t = copy.deepcopy(y_true)
    MRR = 0.0
    MRR_list = []
    Recall = 0.0
    Recall_list = []
    ILD = 0.0
    ILD_list = []
    count = 0
    for i in range(len(hh)):
        pred_list = hh[i]
        target = y_t[i][0]
        count += 1
        if target in pred_list :
            MRR += 1/(pred_list.index(target)+1)
            MRR_list.append(1 / (pred_list.index(target) + 1))
            Recall += 1
            Recall_list.append(1)
        else:
            MRR_list.append(0)
            Recall_list.append(0)
        ILD += cal_ILD(pred_list,item_genre)
        ILD_list.append(cal_ILD(pred_list,item_genre))
    return MRR / count, Recall / count, ILD/ count, MRR_list,Recall_list, ILD_list


def cal_ILD(topk,item_genre):
    id = copy.deepcopy(topk)
    K = len(topk)
    name = item_genre[item_genre['item_id'].isin(id)]
    genre_vector = np.array(name)[:, -19:]
    d = pairwise_distances(genre_vector, metric='euclidean')
    ILD =  np.sum(d)/(K * (K-1))
    return ILD


def save_hparams(hparams, path):
    '''Saves hparams to path
    hparams: argsparse object.
    path: output directory.
    Writes
    hparams as literal dictionary to path.
    '''
    if not os.path.exists(path): os.makedirs(path)
    hp = json.dumps(vars(hparams))
    with open(os.path.join(path, "hparams"), 'w') as fout:
        fout.write(hp)

def load_hparams(parser, path):
    '''Loads hparams and overrides parser
    parser: argsparse parser
    path: directory or file where hparams are saved
    '''
    if not os.path.isdir(path):
        path = os.path.dirname(path)
    d = open(os.path.join(path, "hparams"), 'r').read()
    flag2val = json.loads(d)
    for f, v in flag2val.items():
        parser.f = v

def save_variable_specs(fpath):
    '''Saves information about variables such as
    their name, shape, and total parameter number
    fpath: string. output file path
    Writes
    a text file named fpath.
    '''
    def _get_size(shp):
        '''Gets size of tensor shape
        shp: TensorShape
        Returns
        size
        '''
        size = 1
        for d in range(len(shp)):
            size *=shp[d]
        return size

    params, num_params = [], 0
    for v in tf.compat.v1.global_variables():
        params.append("{}==={}".format(v.name, v.shape))
        num_params += _get_size(v.shape)
    print("num_params: ", num_params)
    with open(fpath, 'w') as fout:
        fout.write("num_params: {}\n".format(num_params))
        fout.write("\n".join(params))
    logging.info("Variables info has been saved.")

def get_hypotheses(num_batches, num_samples, sess, tensor1,tensor2,tensor3):
    hypotheses = []
    true = []
    x_true = []
    for _ in range(num_batches):
        h,t,x_t = sess.run([tensor1,tensor2,tensor3])
        hypotheses.extend(h.tolist())
        true.extend(t.tolist())
        x_true.extend(x_t.tolist())
    return hypotheses[:num_samples],true[:num_samples],x_true[:num_samples]




