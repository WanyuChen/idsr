import argparse

class Hparams:
    parser = argparse.ArgumentParser()

    # prepro
    parser.add_argument('--item_num', default=1349, type=int)


    # train
    ## files
    parser.add_argument('--train', default='../data_pro/data/train',
                             help="train data")
    parser.add_argument('--valid', default='../data_pro/data/valid',
                             help="valid data")
    parser.add_argument('--test', default='../data_pro/data/test',
                             help="test data")

    # training scheme
    parser.add_argument('--batch_size', default=512, type=int)
    parser.add_argument('--eval_batch_size', default=512, type=int)

    parser.add_argument('--lr', default=0.001, type=float, help="learning rate")
    parser.add_argument('--warmup_steps', default=4000, type=int)
    parser.add_argument('--logdir', default="log", help="log directory")
    parser.add_argument('--num_epochs', default=40, type=int)
    parser.add_argument('--evaldir', default="eval/1", help="evaluation dir")

    # model

    parser.add_argument('--hidden_units', default=128, type=int,
                        help="hidden dimension")
    parser.add_argument('--enc_units', default=128, type=int,
                        help="GRU hidden dimension")
    parser.add_argument('--self_attention_mode', default=1, type=int,
                        help="self_attention_mode")
    parser.add_argument('--num_units', default=128, type=int,
                        help="hidden dimension of item embedding")
    parser.add_argument('--k', default=10, type=int,
                        help="length of recommedantion list")
    parser.add_argument('--maxlen', default=9, type=int,
                        help="length of sequence")
    parser.add_argument('--lamb1', default=0.2, type=float)
    parser.add_argument('--lamb2', default=1.0, type=float)
    parser.add_argument('--pos', default=1.0, type=float)

    # test

    parser.add_argument('--ckpt', help="checkpoint file path")
    parser.add_argument('--testdir', default="test_results/1", help="test result dir")
