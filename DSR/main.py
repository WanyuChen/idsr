import tensorflow as tf
from model_RNN import DSR_RNN
from tqdm import tqdm
from data_loader import get_batch
from utils import save_hparams, save_variable_specs, get_hypotheses, evaluate
import os
from hparams import Hparams
import math
import logging
import sys
import copy
import random
import numpy as np
from collections import defaultdict
import pandas as pd
from sklearn.metrics.pairwise import pairwise_distances
from tensorflow.python.util import deprecation

deprecation._PRINT_DEPRECATION_WARNINGS = False
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
from numpy.random import seed
seed(42)
tf.compat.v1.set_random_seed(42)
random.seed(42)

item_genre = pd.read_csv('../data_pro/data/properties.csv')
logging.basicConfig(level=logging.INFO)
logging.info("# hparams")
hparams = Hparams()
parser = hparams.parser
hp = parser.parse_args()
logdir = hp.logdir + str(hp.lamb1) + str(hp.lamb2) + '/'


logging.info("# Prepare train/eval batches")

train_batches, num_train_batches, num_train_samples = get_batch(hp.train, hp.batch_size,
                                                                shuffle=True)
valid_batches, num_valid_batches, num_valid_samples = get_batch(hp.valid, hp.eval_batch_size,
                                                                shuffle=False)
test_batches, num_test_batches, num_test_samples = get_batch(hp.test, hp.eval_batch_size,
                                                             shuffle=False)
iter = tf.data.Iterator.from_structure(train_batches.output_types, train_batches.output_shapes)
xs, ys = iter.get_next()

train_init_op = iter.make_initializer(train_batches)
eval_init_op = iter.make_initializer(valid_batches)
test_init_op = iter.make_initializer(test_batches)

logging.info("# Load model")
m = DSR_RNN(hp)
role_weig, pos_pre, s_rev_1, s_div_1, loss, loss_rel, loss_div, loss_dist_pos, loss_ME, train_op, global_step, train_summaries = m.train(
    xs, ys)
y_hat, y_true, xs_true, s_rel_1_eval, s_div_1_eval, eval_summaries = m.eval(xs, ys)

logging.info("# Session")
saver = tf.train.Saver(max_to_keep=1)
with tf.Session() as sess:
    ckpt = tf.train.latest_checkpoint(logdir)
    if ckpt is None:
        logging.info("# Initializing from scratch")
        sess.run(tf.global_variables_initializer())
        if not os.path.exists(logdir): os.makedirs(logdir)
        save_variable_specs(os.path.join(logdir, "specs"))
    else:
        saver.restore(sess, ckpt)
    sess.run(train_init_op)
    total_steps = hp.num_epochs * num_train_batches
    _gs = sess.run(global_step)
    M_eval = []
    R_eval = []
    I_eval = []
    E_eval = []
    Test = {}
    for i in tqdm(range(_gs, total_steps + 1)):
        _, _gs, _summary = sess.run([train_op, global_step, train_summaries])
        epoch = math.ceil(_gs / num_train_batches)
        if _gs and _gs % num_train_batches == 0:
            r_w, p_pre, s_r, s_d, l, l_r, l_d, l_p, l_m = sess.run(
                [role_weig, pos_pre, s_rev_1, s_div_1, loss, loss_rel, loss_div, loss_dist_pos,
                loss_ME])  # train loss
            logging.info(
                "# epoch {} is done,loss is {}".format(
                    epoch, l))
            logging.info("# validation evaluation")
            _, _eval_summaries = sess.run([eval_init_op, eval_summaries])
            hypotheses, true, x_true = get_hypotheses(num_valid_batches, num_valid_samples, sess, y_hat, y_true,
                                                      xs_true)
            MRR, Recall, ILD, MRR_list, Recall_list, ILD_list = evaluate(hypotheses, true, item_genre)
            logging.info("# validation: MRR is {}, Recall is {}, ILD is {}".format(MRR, Recall, ILD))
            M_eval.append(MRR)
            R_eval.append(Recall)
            I_eval.append(ILD)
            E_eval.append(epoch)
            if Recall >= np.array(R_eval).max():
                logging.info("# save models")
                model_output = "DSR"
                ckpt_name = os.path.join(logdir, model_output)
                saver.save(sess, ckpt_name, global_step=_gs)
                logging.info("# after training of {} epochs, {} has been saved.".format(epoch, ckpt_name))
                logging.info("# fall back to train mode")
            sess.run(train_init_op)
    ckpt_ = tf.train.latest_checkpoint(logdir)
    ckpt = ckpt_ # None: ckpt is a file. otherwise dir.
    saver.restore(sess, ckpt)
    _, _test_summaries = sess.run([test_init_op, eval_summaries])
    hypotheses,true,x_true = get_hypotheses(num_test_batches, num_test_samples, sess, y_hat,y_true,xs_true)
    MRR, Recall, ILD,MRR_list,Recall_list, ILD_list = evaluate(hypotheses, true,item_genre)
    logging.info("# test: MRR is {}, Recall is {}, ILD is {}".format(MRR, Recall, ILD))
    logging.info("Done")