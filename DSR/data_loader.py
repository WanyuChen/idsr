import tensorflow as tf
import pickle
import functools
from numpy.random import seed
seed(42)
tf.compat.v1.set_random_seed(42)
import random
random.seed(42)
def load_data(fpath):
    sents1, sents3 = [], []
    f1 = open(fpath, 'rb')
    train_set = pickle.load(f1)
    for xx, zz in zip(train_set[0],train_set[2]):
        sents1.append(xx)
        sents3.append(zz)

    return sents1, sents3


def generator_fn(fpath):
    with open(fpath, 'rb') as f1:
        train_set = pickle.load(f1)
        for xx, zz in zip(train_set[0], train_set[2]):
            yield xx,zz

def input_fn(fpath, batch_size, shuffle=False):
    shapes = ([None],
              [None])
    types = (tf.int32,
             tf.int32)
    paddings = (tf.constant(0, dtype=tf.int32),
                tf.constant(0, dtype=tf.int32))

    dataset = tf.data.Dataset.from_generator(
        functools.partial(generator_fn, fpath),
        output_shapes=shapes,
        output_types=types)

    if shuffle: # for training
        dataset = dataset.shuffle(128*batch_size)
    dataset = dataset.repeat()  # iterate forever
    dataset = dataset.padded_batch(batch_size, shapes, paddings).prefetch(1)
    return dataset

def get_batch(fpath, batch_size, shuffle=False):
    sents1,sents3 = load_data(fpath)
    batches = input_fn(fpath, batch_size, shuffle=shuffle)
    num_batches = len(sents1) // batch_size + int(len(sents1) % batch_size != 0)

    return batches, num_batches, len(sents1)
