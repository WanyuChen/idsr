from __future__ import print_function
import tensorflow as tf
import numpy as np
import pandas as pd
import copy
from numpy.random import seed
seed(42)
tf.compat.v1.set_random_seed(42)
import random
random.seed(42)

def tensor_ILD(topk):
    item_genre = pd.read_csv('../data_pro/data/properties.csv')
    genre_data = item_genre.T.copy()
    genre_data = genre_data.tail(19).T
    genre_data = np.array(genre_data, dtype=np.float32)
    genre_data = tf.convert_to_tensor(genre_data)
    id = copy.copy(topk)
    K = topk.get_shape().as_list()[-1]
    genre_vector = tf.to_float(tf.nn.embedding_lookup(genre_data, id))
    ILD = 0.0
    for i in range(K):
        genre_vector_0 = tf.reshape(genre_vector[:, i, :],
                                    [tf.shape(genre_vector[:, i, :])[0], 1, tf.shape(genre_vector[:, i, :])[1]])
        ele_1 = tf.to_float(tf.tile(genre_vector_0, [1, tf.shape(genre_vector)[1], 1]))
        ele_1_prod = tf.sqrt(tf.reduce_sum(tf.square(ele_1 - genre_vector), axis=2))
        ILD = ILD + ele_1_prod
    ILD = tf.reduce_sum(ILD, -1) / K
    ILD = ILD / (K - 1)
    return ILD


def new_softmax(x):
    x0 = tf.reduce_sum(x, -1)
    x1 = tf.reshape(x0, [tf.shape(x)[0], 1])
    x2 = tf.tile(x1, [1, tf.shape(x)[1]])
    x3 = x / x2
    return x3

def ln(inputs, epsilon=1e-8, scope="ln"):
    '''Applies layer normalization. See https://arxiv.org/abs/1607.06450.
    inputs: A tensor with 2 or more dimensions, where the first dimension has `batch_size`.
    epsilon: A floating number. A very small number for preventing ZeroDivision Error.
    scope: Optional scope for `variable_scope`.

    Returns:
      A tensor with the same shape and data dtype as `inputs`.
    '''
    with tf.compat.v1.variable_scope(scope, reuse=tf.compat.v1.AUTO_REUSE):
        inputs_shape = inputs.get_shape()
        params_shape = inputs_shape[-1:]

        mean, variance = tf.nn.moments(inputs, [-1], keep_dims=True)
        beta = tf.compat.v1.get_variable("beta", params_shape, initializer=tf.zeros_initializer())
        gamma = tf.compat.v1.get_variable("gamma", params_shape, initializer=tf.ones_initializer())
        normalized = (inputs - mean) / ((variance + epsilon) ** (.5))
        outputs = gamma * normalized + beta

    return outputs


def mask(inputs, queries=None, keys=None, type=None):
    """Masks paddings on keys or queries to inputs
    inputs: 3d tensor. (N, T_q, T_k)
    queries: 3d tensor. (N, T_q, d)
    keys: 3d tensor. (N, T_k, d)
    e.g.,
    >> queries = tf.constant([[[1.],
                        [2.],
                        [0.]]], tf.float32) # (1, 3, 1)
    >> keys = tf.constant([[[4.],
                     [0.]]], tf.float32)  # (1, 2, 1)
    >> inputs = tf.constant([[[4., 0.],
                               [8., 0.],
                               [0., 0.]]], tf.float32)
    >> mask(inputs, queries, keys, "key")
    array([[[ 4.0000000e+00, -4.2949673e+09],
        [ 8.0000000e+00, -4.2949673e+09],
        [ 0.0000000e+00, -4.2949673e+09]]], dtype=float32)
    >> inputs = tf.constant([[[1., 0.],
                             [1., 0.],
                              [1., 0.]]], tf.float32)
    >> mask(inputs, queries, keys, "query")
    array([[[1., 0.],
        [1., 0.],
        [0., 0.]]], dtype=float32)
    """
    padding_num = -2 ** 32 + 1
    if type in ("k", "key", "keys"):
        # Generate masks
        masks = tf.sign(tf.reduce_sum(tf.abs(keys), axis=-1))  # (N, T_k)
        masks = tf.expand_dims(masks, 1)  # (N, 1, T_k)
        masks = tf.tile(masks, [1, tf.shape(queries)[1], 1])  # (N, T_q, T_k)

        # Apply masks to inputs
        paddings = tf.ones_like(inputs) * padding_num
        outputs = tf.where(tf.equal(masks, 0), paddings, inputs)  # (N, T_q, T_k)
    elif type in ("q", "query", "queries"):
        # Generate masks
        masks = tf.sign(tf.reduce_sum(tf.abs(queries), axis=-1))  # (N, T_q)
        masks = tf.expand_dims(masks, -1)  # (N, T_q, 1)
        masks = tf.tile(masks, [1, 1, tf.shape(keys)[1]])  # (N, T_q, T_k)

        # Apply masks to inputs
        outputs = inputs * masks
    elif type in ("f", "future", "right"):
        diag_vals = tf.ones_like(inputs[0, :, :])  # (T_q, T_k)
        tril = tf.linalg.LinearOperatorLowerTriangular(diag_vals).to_dense()  # (T_q, T_k)
        masks = tf.tile(tf.expand_dims(tril, 0), [tf.shape(inputs)[0], 1, 1])  # (N, T_q, T_k)

        paddings = tf.ones_like(masks) * padding_num
        outputs = tf.where(tf.equal(masks, 0), paddings, inputs)
    else:
        print("Check if you entered type correctly!")

    return outputs



def noam_scheme(init_lr, global_step, warmup_steps=4000.):
    '''Noam scheme learning rate decay
    init_lr: initial learning rate. scalar.
    global_step: scalar.
    warmup_steps: scalar. During warmup_steps, learning rate increases
        until it reaches init_lr.
    '''
    step = tf.cast(global_step + 1, dtype=tf.float32)
    return init_lr * warmup_steps ** 0.5 * tf.minimum(step * warmup_steps ** -1.5, step ** -0.5)



def get_item_embeddings(vocab_size, num_units, zero_pad=True):
    with tf.compat.v1.variable_scope("shared_item_matrix", reuse=tf.compat.v1.AUTO_REUSE):
        embeddings = tf.compat.v1.get_variable('weight_mat',
                                     dtype=tf.float32,
                                     shape=(vocab_size, num_units),
                                     initializer=tf.contrib.layers.xavier_initializer())
        if zero_pad:
            embeddings = tf.concat((tf.zeros(shape=[1, num_units]),
                                    embeddings[1:, :]), 0)
    return embeddings



def self_attention(xsss, Q, K, V, size_head, mode, scope):
    with tf.compat.v1.variable_scope(scope, reuse=tf.compat.v1.AUTO_REUSE):
        padding_num = -2 ** 32 + 1
        if mode == 0:
            Q = tf.layers.dense(Q, size_head, use_bias=False)
            Q = tf.reshape(Q, (-1, tf.shape(Q)[1], size_head))
            K = tf.layers.dense(K, size_head, use_bias=False)
            K = tf.reshape(K, (-1, tf.shape(K)[1], size_head))
            V = tf.layers.dense(V, size_head, use_bias=False)
            V = tf.reshape(V, (-1, tf.shape(V)[1], size_head))

            A0 = tf.matmul(Q, K, transpose_b=True) / tf.sqrt(
                float(size_head))
            A1 = tf.reshape(A0, [tf.shape(A0)[0], -1, 1])

            paddings = tf.ones_like(A1) * padding_num
            outputs = tf.where(tf.equal(xsss, 0), paddings, A1)
            A2 = tf.nn.softmax(outputs, 1)

            O = tf.reduce_sum(A2 * V, 1)
            O = tf.reshape(O, [tf.shape(Q)[0], 1, size_head])
            A = tf.reshape(A2, [tf.shape(K)[0], 1, tf.shape(K)[1]])

            return O, A, V, Q

        if mode == 1:
            Q = tf.layers.dense(Q, size_head, use_bias=False)
            Q = tf.reshape(Q, (-1, tf.shape(Q)[1], size_head))

            K = tf.layers.dense(K, size_head, use_bias=False)
            K = tf.reshape(K, (-1, tf.shape(K)[1], size_head))

            V = tf.layers.dense(V, size_head, use_bias=False)
            V = tf.reshape(V, (-1, tf.shape(V)[1], size_head))
            W = tf.compat.v1.get_variable("weight_1", dtype=tf.float32, shape=(Q.get_shape()[-1], K.get_shape()[-1]),
                                initializer=tf.contrib.layers.xavier_initializer())
            QW = tf.matmul(tf.reshape(Q, (-1, tf.shape(Q)[-1])), W)
            QW = tf.reshape(QW, (-1, tf.shape(Q)[1], tf.shape(Q)[-1]))
            QWK = tf.matmul(QW, tf.transpose(K, [0, 2, 1])) / tf.sqrt(float(size_head))
            A = tf.reshape(QWK, [tf.shape(K)[0], tf.shape(K)[1], 1])
            paddings = tf.ones_like(A) * padding_num
            outputs = tf.where(tf.equal(xsss, 0), paddings, A)
            A2 = tf.nn.softmax(outputs, 1)

            O = tf.reduce_sum(A2 * V, 1)
            O = tf.reshape(O, [tf.shape(Q)[0], 1, size_head])
            A = tf.reshape(A2, [tf.shape(K)[0], 1, tf.shape(K)[1]])
            return O, A, V, Q

        if mode == 2:
            Q0 = tf.reshape(Q, [tf.shape(Q)[0], size_head])

            W_A = tf.compat.v1.get_variable('W_A', dtype=tf.float32, shape=(size_head, size_head),
                                  initializer=tf.contrib.layers.xavier_initializer())
            W_B = tf.compat.v1.get_variable('W_B', dtype=tf.float32, shape=(size_head, size_head),
                                  initializer=tf.contrib.layers.xavier_initializer())
            W_V = tf.compat.v1.get_variable('W_V', dtype=tf.float32, shape=(1, size_head),
                                  initializer=tf.contrib.layers.xavier_initializer())
            K1 = tf.reshape(K, [-1, size_head]) 
            l11 = tf.reshape(tf.matmul(W_A, tf.transpose(K1)),
                             [-1, tf.shape(K)[0], tf.shape(K)[1]])  
            l22 = tf.reshape(tf.matmul(W_B, tf.transpose(Q0)), [-1, tf.shape(K)[0], 1])  
            l22 = tf.tile(l22, [1, 1, tf.shape(K)[1]]) 
            l33 = tf.reshape(tf.keras.backend.hard_sigmoid(l11 + l22), [tf.shape(l22)[0], -1])  
            q_jh = tf.reshape(tf.matmul(W_V, l33), [tf.shape(K)[0], tf.shape(K)[1], 1])  
            paddings = tf.ones_like(q_jh) * padding_num
            outputs = tf.where(tf.equal(xsss, 0), paddings, q_jh)
            att = tf.nn.softmax(outputs, 1)  
            A2 = att  

            O = tf.reduce_sum(A2 * V, 1)  
            O = tf.reshape(O, [tf.shape(Q)[0], 1, size_head])
            A = tf.reshape(A2, [tf.shape(K)[0], 1, tf.shape(K)[1]])
            return O, A, V, Q


def warmup_scheme(init_lr, global_step, num_warmup_steps, LEARNING_RATE_STEP=153, LEARNING_RATE_DECAY=0.99,
                  warmup=True):
    """
    decay learning rate
    if warmup > global step, the learning rate will be global_step/num_warmup_steps * init_lr
    if warmup < global step, the learning rate will be polynomial decay
    :param global_step: global steps
    :param num_warmup_steps: number of warm up steps
    :param num_train_steps: number of train steps
    :param init_lr: initial learning rate
    :param warmup: if True, it will warm up learning rate
    :return: learning rate
    """
    learning_rate = tf.constant(value=init_lr, shape=[], dtype=tf.float32)
    tf.train.exponential_decay(learning_rate,
                               global_step,
                               LEARNING_RATE_STEP,
                               LEARNING_RATE_DECAY,
                               staircase=True)

    if warmup:
        global_steps_int = tf.cast(global_step, tf.int32)
        warmup_steps_int = tf.constant(num_warmup_steps, dtype=tf.int32)

        global_steps_float = tf.cast(global_steps_int, tf.float32)
        warmup_steps_float = tf.cast(warmup_steps_int, tf.float32)

        warmup_percent_done = global_steps_float / warmup_steps_float
        warmup_learning_rate = init_lr * warmup_percent_done

        is_warmup = tf.cast(global_steps_int < warmup_steps_int, tf.float32)
        learning_rate = ((1.0 - is_warmup) * learning_rate + is_warmup * warmup_learning_rate)

    return learning_rate
